// Base Imports
import './App.css';
import { useState } from 'react';

// Dependencies
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';

// Local Imports
import AppNavbar from './components/AppNavbar';
import ProductView from './components/ProductView';
import Footer from './components/Footer';

import AddProduct from './pages/AddProduct';
import Cart from './pages/Cart';
import ErrorPage from './pages/ErrorPage';
import Home from './pages/Home';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Orders from './pages/Orders';
import OrderView from './pages/OrderView';
import Products from './pages/Products';
import Profile from './pages/Profile';
import Register from './pages/Register';
import Users from './pages/Users';

import { UserProvider } from './UserContext';


function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })

  const unsetUser = () => {
    localStorage.clear()
  }

  return (
      <>
         {/*Provides the user context throughout any component inside of it.*/}
          <UserProvider value={{user, setUser, unsetUser}}>
            {/*Initializes that dynamic routing will be involved*/}
              <Router>
                  <AppNavbar/>
                  <Container>       
                      <Routes>
                          <Route path="/" element={<Home/>}/>
                          <Route path="/products/create" element={<AddProduct/>}/>
                          <Route path="/cart" element={<Cart/>}/>
                          <Route path="/products/:productId" element={<ProductView/>}/>
                          <Route path="/login" element={<Login/>}/>
                          <Route path="/logout" element={<Logout/>}/>
                          <Route path="/orders" element={<Orders/>}/>
                          <Route path="/orders/:orderId" element={<OrderView/>}/>
                          <Route path="/products" element={<Products/>}/>
                          <Route path="/register" element={<Register/>}/>
                          <Route path="/profile" element={<Profile/>}/>
                          <Route path="/users" element={<Users/>}/>
                          <Route path="*" element={<ErrorPage/>}/>
                      </Routes>
                  </Container>
                  <Footer/>
                </Router>   
          </UserProvider>   
      </>
    );
}

export default App;
