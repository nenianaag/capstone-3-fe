// Base imports
import {useState, useEffect, useContext} from 'react';
import {Link, NavLink} from 'react-router-dom';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faHouse, faCartShopping, faCircleUser, faUser, faList, faBasketShopping, faRightFromBracket, faLeaf, faAddressBook, faTags } from '@fortawesome/free-solid-svg-icons'


// Local Imports
import UserContext from '../UserContext'


export default function AppNavbar(){

	const {user} = useContext(UserContext)


	return(
		<Navbar collapseOnSelect bg="success" expand="lg">
			<Navbar.Brand as={Link} to="/" className="ms-3"><span className="text-light">Amazing<FontAwesomeIcon className="mx-2" icon={faLeaf} />Bamboo</span></Navbar.Brand>
			<Navbar.Toggle aria-controls="basic-navbar-nav" />
			<Navbar.Collapse id="basic-navbar-nav">
				<Nav className="ms-auto me-3">
					<Nav.Link eventKey="1" className="ms-3 ms-md-3 ms-lg-0 text-light" as={NavLink} to="/"><FontAwesomeIcon className="me-2" icon={faHouse} />Home</Nav.Link>
					<Nav.Link eventKey="1" className="ms-3 ms-md-3 ms-lg-0 text-light" as={NavLink} to="/products"><FontAwesomeIcon className="me-2" icon={faBasketShopping} />{user.isAdmin === true ? "Products" : "Shop"}</Nav.Link>
					{ (user.id) ?
						user.isAdmin === true ?
							<>
								<Nav.Link eventKey="1" className="ms-3 ms-md-3 ms-lg-0 text-light" as={NavLink} to="/orders"><FontAwesomeIcon className="me-2" icon={faList} />Orders</Nav.Link>
								<Nav.Link eventKey="1" className="ms-3 ms-md-3 ms-lg-0 text-light" as={NavLink} to="/products/create"><FontAwesomeIcon className="me-2" icon={faTags} />Add Product</Nav.Link>
								<Nav.Link eventKey="1" className="ms-3 ms-md-3 ms-lg-0 text-light" as={NavLink} to="/users"><FontAwesomeIcon className="me-2" icon={faAddressBook} />Users</Nav.Link>
								<Nav.Link eventKey="1" className="ms-3 ms-md-3 ms-lg-0 text-light" as={NavLink} to="/profile"><FontAwesomeIcon className="me-2" icon={faUser} />Profile</Nav.Link>
								<Nav.Link eventKey="1" className="ms-3 ms-md-3 ms-lg-0 text-light" as={NavLink} to="/logout"><FontAwesomeIcon className="me-2" icon={faRightFromBracket} />Logout</Nav.Link>
							</>
							:
							<>
								<Nav.Link eventKey="1" className="ms-3 ms-md-3 ms-lg-0 text-light" as={NavLink} to="/orders"><FontAwesomeIcon className="me-2" icon={faList} />Orders</Nav.Link>
								<Nav.Link eventKey="1" className="ms-3 ms-md-3 ms-lg-0 text-light" as={NavLink} to="/cart"><FontAwesomeIcon className="me-2" icon={faCartShopping} />Cart</Nav.Link>
								<Nav.Link eventKey="1" className="ms-3 ms-md-3 ms-lg-0 text-light" as={NavLink} to="/profile"><FontAwesomeIcon className="me-2" icon={faUser} />Profile</Nav.Link>
								<Nav.Link eventKey="1" className="ms-3 ms-md-3 ms-lg-0 text-light" as={NavLink} to="/logout"><FontAwesomeIcon className="me-2" icon={faRightFromBracket} />Logout</Nav.Link>
							</>
						:
						<>
							<Nav.Link eventKey="1" className="ms-3 ms-md-3 ms-lg-0 text-light" as={NavLink} to="/login"><FontAwesomeIcon className="me-2" icon={faCircleUser} />Login</Nav.Link>
							<Nav.Link eventKey="1" className="ms-3 ms-md-3 ms-lg-0 text-light" as={NavLink} to="/register"><FontAwesomeIcon className="me-2" icon={faUser} />Register</Nav.Link>	
						</>
					}							
				</Nav>
			</Navbar.Collapse>
		</Navbar>	
	)
}