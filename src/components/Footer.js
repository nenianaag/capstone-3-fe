export default function Footer(){

	return (
		<div className="footer text-center text-light position-fixed w-100 bottom-0 bg-success">
			<div className="container">
				<p className="my-1">All assets are not owned by me and are only used for educational purposes only.</p>
			</div>
		</div>
	)
  
};