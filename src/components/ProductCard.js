import { useState, useEffect, useContext } from 'react'
import { Card, Button, Form } from 'react-bootstrap'
import Proptypes from 'prop-types'
import { Link } from 'react-router-dom'
import Swal from 'sweetalert2'

import UpdateProductModal from './UpdateProductModal'
import UserContext from '../UserContext'

export default function ProductCard({product, rerenderProducts, setRerenderProducts}){

	const {user, setUser} = useContext(UserContext)

	const {_id} = product

	const [name, setName] = useState('');
	const [category, setCategory] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');
	const [isActive, setIsActive] = useState(true);
	const [stocks, setStocks] = useState(0);
	const [isAvailable, setIsAvailable] = useState(isActive);
	const [isUpdating, setIsUpdating] = useState(false);
	const [rerender, setRerender] = useState(false);
	console.log(localStorage.getItem("isAdmin"));

	let toggleUpdateModal = () => {
		setIsUpdating(!isUpdating)
		setRerenderProducts(!rerenderProducts)
	}

	let archiveProduct = () => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${_id}/archive`, {
			method: 'DELETE',
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(response => response.json())
		.then(result => {
			if(result === true) {
				setRerender(!rerender)
				Swal.fire({
					title: "Nice!",
					icon: "success",
					text: "Operation successful!"
				})
			} else {
				Swal.fire({
					title: "Ooopps!",
					icon: "error",
					text: "Operation unsuccessful!"
				})
			}			
		})
	}

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${_id}`)
		.then(response => response.json())
		.then(result => {

			setName(result.name)
			setCategory(result.category);
			setDescription(result.description)
			setPrice(result.price)
			setIsActive(result.isActive)
			setStocks(result.stocks)

		})
	}, [rerender, isUpdating])

	return(
		<Card className="col-lg-3 col-md-5 mx-md-3 my-3">
			<Card.Body className="text-center">
				<Card.Img variant="top" src={process.env.PUBLIC_URL + '/toothpaste.png'} />
				<Card.Title className="mt-3">{name}</Card.Title>
				<Card.Text>{description}</Card.Text>
				<Card.Subtitle>Price:</Card.Subtitle>
				<Card.Text>Php {price}</Card.Text>
				<Card.Text>Available Stock: {stocks}</Card.Text>
				<div className="text-center">
					<Link className="btn btn-primary mx-1" to={`/products/${_id}`}>Details</Link>
				</div>
				
				<div className="text-center my-1">
					{ user.isAdmin === true ?
					<Button className="btn btn-warning mx-1" onClick={toggleUpdateModal}>Update</Button>
					:
					null
				}
				</div>
				
				<div className="text-center">
					{user.isAdmin === true ?
					isActive ?
						<Button className="btn btn-danger mx-1" onClick={archiveProduct}>Archive</Button>
							
					:
						<Button className="btn btn-success mx-1" onClick={archiveProduct}>Unarchive</Button>
				:
					null	
				}
				</div>
				
			</Card.Body>

			{ isUpdating ?
				<UpdateProductModal show={isUpdating} toggleUpdateModal={toggleUpdateModal} productId={_id}/>
				:
				null
			}
		</Card>
	)


}