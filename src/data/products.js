const products_data = [
	{
		id: "bc01",
		name: "Bamboo Charcoal",
		category: "home-essentials",
		description: "A charcoal made from the species of Bamboo.",
		price: 20,
		isAvailable: true
	},
	{
		id: "bcf02",
		name: "Bamboo Charcoal Filter",
		category: "home-essentials",
		description: "A small pouch containing bamboo powder useful for preventing foul odor",
		price: 50,
		isAvailable: true
	},
	{
		id: "bct03",
		name: "Bamboo Charcoal Toothpaste",
		category: "personal-care",
		description: "Removes surface stains, and whitens teeth with a refreshingly minty taste",
		price: 200,
		isAvailable: true
	},
	{
		id: "bctwp04",
		name: "Bamboo Charcoal Teeth Whitening Powder",
		category: "personal-care",
		description: "It will help to strengthen your gums, remove toxins and bad smell from your mouth.",
		price: 100,
		isAvailable: true
	},
	{
		id: "bcfc05",
		name: "Bamboo Charcoal Face Soap",
		category: "personal-care",
		description: "Natural bamboo extracts unhealthy toxins from beneath your skin to prevent clogged pores, breakouts, and skin irritations.",
		price: 80,
		isAvailable: true
	},
	{
		id: "bcbs06",
		name: "Bamboo Charcoal Body Soap",
		category: "personal-care",
		description: "Feel fresh and cleansed with this all-natural, vegan and handmade bamboo charcoal soap that can detoxify your skin and your body.",
		price: 150,
		isAvailable: true
	},
	{
		id: "bcpom07"
		name: "Bamboo Charcoal Peel-off Mask",
		category: "personal-care",
		description: "Made with Charcoal and minerals; natural ingredients that help clean dirt and oil from pores allowing moisture to be absorbed into the skin.",
		price: 120,
		isAvailable: true
	},
	{
		id: "bcpom08"
		name: "Bamboo Charcoal Toothbrush",
		category: "personal-care",
		description: "Biodegradable Eco-Friendly Natural Bamboo Charcoal Toothbrush.",
		price: 80,
		isAvailable: true
	}
]

export default products_data