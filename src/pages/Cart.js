import { useState, useEffect, useContext } from 'react'
import { Table, Button, Form } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTrashCan } from '@fortawesome/free-solid-svg-icons'

// Local Imports
import UserContext from '../UserContext'

export default function Cart() {

	const {user, setUser} = useContext(UserContext)
	const navigate = useNavigate()

	const [cartData, setCartData] = useState({});
	const [cartRows, setCartRows] = useState([]);
	const [total, setTotal] = useState(0);
	const [address, setAddress] = useState('')
	const [paymentMethod, setPaymentMethod] = useState('')

	let updateCartQty = (e, cartKey) => {
		let data = {...cartData}
		let amount = parseInt(e.target.value)
		if (e.target.value === '' || e.target.value === '0') {
			amount = 1
		} else {
			data[cartKey].quantity = amount;
			data[cartKey].subtotal = amount * data[cartKey].price
			console.log(data)
			setCartData(data)
			fetch(`${process.env.REACT_APP_API_URL}/carts/${data[cartKey].cartId}`, {
				method: 'PUT',
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
					cartId: data[cartKey].cartId,
					quantity: data[cartKey].quantity
				})
			})
			.then(response => response.json())
			.then(result => {
				console.log(result)
			})
		}
	}

	let subtractQty = (cartKey) => {
		let data = {...cartData}
		if(data[cartKey].quantity > 1) {
			data[cartKey].quantity -= 1
			data[cartKey].subtotal = data[cartKey].quantity * data[cartKey].price
			setCartData(data)
			fetch(`${process.env.REACT_APP_API_URL}/carts/${data[cartKey].cartId}`, {
				method: 'PUT',
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
					cartId: data[cartKey].cartId,
					quantity: data[cartKey].quantity
				})
			})
			.then(response => response.json())
			.then(result => {
				console.log(result)
			})
		}
	}

	let addQty = (cartKey) => {
		let data = {...cartData}

		if(data[cartKey].quantity >= data[cartKey].stocks) {
			Swal.fire({
				title: 'Invalid Amount!',
				icon: 'error',
				text: 'You cannot add more than the available stocks!'
			})
		} else {
			data[cartKey].quantity += 1
			data[cartKey].subtotal = data[cartKey].quantity * data[cartKey].price
			setCartData(data)
			fetch(`${process.env.REACT_APP_API_URL}/carts/${data[cartKey].cartId}`, {
				method: 'PUT',
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
					cartId: data[cartKey].cartId,
					quantity: data[cartKey].quantity
				})
			})
			.then(response => response.json())
			.then(result => {
				console.log(result)
			})
		}
	}

	let checkout = (e) => {

		e.preventDefault();
		if(paymentMethod === ""){
			Swal.fire({
					title: "Payment Method Required!",
					icon: 'error',
					text: "Please choose a payment method."
				})	
		} else {
			let data = {...cartData}
			fetch(`${process.env.REACT_APP_API_URL}/carts/checkout`, {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
					userId: user.id,
					deliveryAddress: address,
					paymentMethod: paymentMethod 
				})
			})
			.then(response => response.json())
			.then(result => {
				console.log(result)

				if (result === true) {
					Swal.fire({
						title: "Checkout successful!",
						icon: 'success',
						text: "The products have been successfully checked out. Thank you for shopping!"
					})
					navigate('/orders')
				} else {
					Swal.fire({
						title: "Ooppss!",
						icon: 'error',
						text: "Something went wrong"
					})
				}
			})
		}	
	}


	let deleteCartProduct = (cartKey) => {
		let data = {...cartData}
		console.log(data[cartKey].cartId)
		fetch(`${process.env.REACT_APP_API_URL}/carts/${data[cartKey].cartId}`, {
			method: 'DELETE',
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(response => response.json())
		.then(result => {
			console.log(result)
			if (result === true) {
				Swal.fire({
					title: "Cart updated!",
					icon: 'success',
					text: "Cart product successfully removed!"
				})
				delete data[cartKey];
				setCartData(data);
			} else {
				Swal.fire({
					title: "Ooppss!",
					icon: 'error',
					text: "Something went wrong"
				})
			}
		})
	}

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/carts/getCartByUserId`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(response => response.json())
		.then(result => {
			console.log(result)

			let completeData = result.map((cart) => {
				return fetch(`${process.env.REACT_APP_API_URL}/products/${cart.productId}`, {
					headers: {
						Authorization: `Bearer ${localStorage.getItem('token')}`
					}
				})
				.then(response => response.json())
				.then(result => {
					console.log(result)

					return {
						cartId: cart._id,
						name: result.name,
						description: result.description,
						price: result.price,
						stocks: result.stocks,
						quantity: cart.quantity,
						subtotal: cart.quantity * result.price
					}
				})
			})

			Promise.all(completeData)
			.then(result => {
				console.log(result)

				let finalData = {}

				result.forEach((cart) => {
					finalData[cart.name] = {
						cartId: cart.cartId,
						description: cart.description,
						price: cart.price,
						stocks: cart.stocks,
						quantity: cart.quantity,
						subtotal: cart.subtotal
					}
				})

				console.log(finalData)
				setCartData(finalData)
			})

		})
	}, [])

	useEffect(() => {
		console.log(cartData);

		let totalAmount = 0;

		let cartKeys = Object.keys(cartData);

		let cartElements = cartKeys.map((cartKey) => {

			totalAmount += cartData[cartKey].subtotal;

			return (
				<tr key={cartKey}>
					<td>{cartKey}</td>
					<td>{cartData[cartKey].description}</td>
					<td>{cartData[cartKey].price}</td>
					<td>
						<div className="d-flex justify-content-center">
							<div className="qty-control d-flex">
								<Button 
									className="btn btn-warning qty-btn"
									onClick={() => subtractQty(cartKey)}
								>-</Button>
								
								<input
									className="qty-input text-center"
									type="text"
									value={cartData[cartKey].quantity}
									onChange={(e) => updateCartQty(e, cartKey)}
								></input>

								<Button 
									className="btn btn-warning qty-btn"
									onClick={() => addQty(cartKey)}
								>+</Button>
							</div>
						</div>
					</td>
					<td>{cartData[cartKey].subtotal}</td>
					<td>
						<Button 
							className="btn btn-danger"
							onClick={() => deleteCartProduct(cartKey)}
						><FontAwesomeIcon className="mx-2" icon={faTrashCan} /></Button>
					</td>
				</tr>
			)
		})

		setTotal(totalAmount);
		setCartRows(cartElements);
	}, [cartData])

	return (
		<div className="container">
			<div className="row">
				<h1 className="text-center my-3">Cart Page</h1>
				<Table striped bordered hover className="text-center">
					<thead>
						<tr>
							<th>Name</th>
							<th>Description</th>
							<th>Price</th>
							<th>Quantity</th>
							<th>Subtotal</th>
							<th>Actions</th>
						</tr>
					</thead>
					<tbody>
						{ cartRows.length > 0 ?
							cartRows
							:
							<tr>
								<td colSpan="6">
									Cart is Empty
								</td>
							</tr>
						}
						<tr>
							<td className="text-end fw-bold mr-3" colSpan="4">Total</td>
							<td>{total}</td>
							<td></td>
						</tr>
					</tbody>
				</Table>
				<Form onSubmit={event => checkout(event)} className="col-12 col-md-6 offset-md-3 col-lg-8 offset-lg-2">
		            <Form.Group controlId="deliveryAddress">
		                <Form.Label className="fw-bold">Delivery Address</Form.Label>
		                <Form.Control 
			                as="textarea"
			                placeholder="Enter delivery address" 
			                value={address}
			                onChange={event => setAddress(event.target.value)}
			                required
			                rows="3"
		                />      
		            </Form.Group>

		           	<Form.Group className="mb-3">
			           	<div>
			           		<Form.Label className="fw-bold">Payment Method:</Form.Label>
			           	</div>      	
			           	<Form.Check
			           		inline
			           		label="Cash On Delivery"
			           		name="payment-method"
			           		type="radio"
			           		id="cod"
			           		value="cod"
			           		onChange={(e) => setPaymentMethod(e.target.value)}
			           	/>
			           	<Form.Check
			           		inline
			           		label="E-Wallet"
			           		name="payment-method"
			           		type="radio"
			           		id="e-wallet"
			           		value="e-wallet"
			           		onChange={(e) => setPaymentMethod(e.target.value)}
			           	/>
			           	<Form.Check
			           		inline
			           		label="Credit Card"
			           		name="payment-method"
			           		type="radio"
			           		id="credit-card"
			           		value="credit-card"
			           		onChange={(e) => setPaymentMethod(e.target.value)}
			           	/>
		           	</Form.Group>

		            <div className="text-center">
		            	<Button 
		            		className="btn btn-success"
		            		type="submit"
		            	>
		            		Checkout
		            	</Button>
		            </div>      
		        </Form>
			</div>
		</div>
	)
}