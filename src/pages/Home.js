import { Link } from 'react-router-dom'

export default function Home() {
	return (
		<>
		<div className="position-relative text-center">

			<img className="img-fluid home-img my-1 overflow-hidden mt-3 mb-3 mb-md-0" src={process.env.PUBLIC_URL + '/ernie-bg-4a.png'} alt="ernie-bg-4a" />

			<div className="container">
				<div className="row g-4 justify-content-center my-md-3">
					<div className="card col-12 col-md-12 col-lg-5 mx-lg-3 p-0">
					  <div className="row g-0">
					    <div className="col-4 col-md-4">
					      <img src={process.env.PUBLIC_URL + '/filter.png'} className="img-fluid rounded-start" alt="filter"/>
					    </div>
					    <div className="col-8 col-md-8 d-flex justify-content-center align-items-center">
					      <div className="card-body">
					        <h5 className="card-title">Home Essentials</h5>
					        <p>Everything you require to make your life at home more comfortable.</p>
					        <Link to="/products" className="btn btn-success p-1">View More</Link>
					      </div>
					    </div>
					  </div>
					</div>
					<div className="card col-12 col-md-12 col-lg-5 mx-lg-3 p-0">
					  <div className="row g-0">
					    <div className="col-4 col-md-4">
					      <img src={process.env.PUBLIC_URL + '/peel-off-mask.png'} className="img-fluid rounded-start" alt="filter"/>
					    </div>
					    <div className="col-8 col-md-8 d-flex justify-content-center align-items-center">
					      <div className="card-body">
					        <h5 className="card-title">Personal Care</h5>
					        <p>Everything for your personal hygiene needs.</p>
					        <Link to="/products" className="btn btn-success p-1">View More</Link>
					      </div>
					    </div>
					  </div>
					</div>
					<div className="card col-12 col-md-12 col-lg-5 mx-lg-3 p-0 d-lg-none">
					  <div className="row g-0">
					    <div className="col-4 col-md-4">
					      <img src={process.env.PUBLIC_URL + '/shampoo-bar.png'} className="img-fluid rounded-start" alt="filter"/>
					    </div>
					    <div className="col-8 col-md-8 d-flex justify-content-center align-items-center">
					      <div className="card-body">
					        <h5 className="card-title">Hair Care</h5>
					        <Link to="/products" className="btn btn-success p-1">See Details</Link>
					      </div>
					    </div>
					  </div>
					</div>
				</div>
			</div>			
		</div>		
		</>
	)
}