// Base Imports
import { useState, useEffect, useContext } from 'react'
import { useNavigate, Navigate } from 'react-router-dom';
// Dependencies
import { Form, Button, Card } from 'react-bootstrap';
import Swal from 'sweetalert2'
// Local Imports
import UserContext from '../UserContext'


export default function Login(){
	const {user, setUser} = useContext(UserContext)

	const [email, setEmail] = useState('')
	const [password, setPassword] = useState('')
	// Initilize useNavigate
	const navigate = useNavigate()

	const [isActive, setIsActive] = useState(false)

	const retrieveUser = (token) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/details/info`, {
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(response => response.json())
		.then(result => {
			console.log(result)

			setUser({
				id: result._id,
				isAdmin: result.isAdmin
			})
		})
	}

	function authenticate(event){
		event.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(response => response.json())
		.then(result => {
			if(typeof result.accessToken !== "undefined"){
				localStorage.setItem('token', result.accessToken)

				retrieveUser(result.accessToken)

				Swal.fire({
					title: "You've login successfully!",
					icon: 'success',
					text: "Welcome to Amazing Bamboo Family"
				})

				navigate("/")

			} else {
				Swal.fire({
					title: 'Authentication Failed!',
					icon: 'error',
					text: 'Please enter valid details and try again. Thank you!'
				})
			}
		})
	}

	useEffect(() => {
		if((email !== '' && password !== '')) {
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [email, password])

	return(
		(user.id !== null) ?
			<Navigate to="/"/>
		:
		<div className="container">
			<div className="mt-4 row justify-content-center align-items-center">
				<div className="col-12 col-md-6 col-lg-5 p-md-4 ps-lg-0 d-flex justify-content-center align-items-center offset-lg-1 mx-4">
					<img className="img-fluid" src={process.env.PUBLIC_URL + '/ernie-bg-4.png'} alt="ernie-bg-4" />
				</div>
		        <div className="card col-12 col-md-6 col-lg-5 p-md-5 p-lg-3 d-flex flex-column justify-content-center align-items-center shadow mx-4 mt-5 mt-md-0">
		        	<h1 className="text-center">Login</h1>
					<Form onSubmit={event => authenticate(event)}>
			            <Form.Group controlId="userEmail">
			                <Form.Label>Email address</Form.Label>
			                <Form.Control 
				                type="email" 
				                placeholder="Enter email" 
				                value={email}
				                onChange={event => setEmail(event.target.value)}
				                required
			                />		                
			            </Form.Group>

			            <Form.Group className="my-3" controlId="password">
			                <Form.Label>Password</Form.Label>
			                <Form.Control 
				                type="password" 
				                placeholder="Password" 
				                value={password}
				                onChange={event => setPassword(event.target.value)}
				                required
			                />
			                <Form.Text className="text-muted">
			                    We'll never share your email with anyone else.
			                </Form.Text>
			            </Form.Group>

			            <div className="text-center my-3">
				            {	isActive ? 
				            	<Button variant="primary" type="submit" id="submitBtn">
				            		Submit
				            	</Button>
				            	: 
				            	<Button variant="primary" type="submit" id="submitBtn" disabled>
				            		Submit
				            	</Button>
				            }
			            </div>          
			        </Form>
			    </div>
			</div>
		</div>	
	)
}
