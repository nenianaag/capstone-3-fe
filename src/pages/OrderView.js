import { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { Table } from 'react-bootstrap'

export default function OrderView(){

	const {orderId} = useParams();

	const [orderProducts, setOrderProducts] = useState([])
	const [productRows, setProductRows] = useState([])
	const [name, setName] = useState('')	
	const [transactionDate, setTransactionDate] = useState('')
	const [status, setStatus] = useState('')
	const [total, setTotal] = useState(0)

	useEffect(()=>{
		fetch(`${process.env.REACT_APP_API_URL}/orders/${orderId}`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(response => response.json())
		.then(result => {
			console.log(result)
			setTransactionDate(result.purchasedOn)
			setStatus(result.status)
			setTotal(result.totalAmount)

			let products = result.products.map((product) => {
				console.log(product)
				return fetch(`${process.env.REACT_APP_API_URL}/products/${product.productId}`)
				.then(response => response.json())
				.then(indivProduct => {
					console.log(indivProduct)

					return {
						name: indivProduct.name,
						price: indivProduct.price,
						quantity: product.quantity
					}

				})
			})

			Promise.all(products)
			.then(result => {
				setOrderProducts(result)
			})

			fetch(`${process.env.REACT_APP_API_URL}/users/${result.userId}`, {
				headers: {
					Authorization: `Bearer ${localStorage.getItem('token')}`
				}
			})
			.then(response => response.json())
			.then(result => {
				console.log(result)
				setName(`${result.firstName} ${result.lastName}`)
			})
		})
	}, [])

	useEffect(() => {
		let productComponents = orderProducts.map((product) => { console.log(product)
			return (
				<tr key={product.name}>
					<td>{product.name}</td>
					<td>{product.quantity}</td>
					<td>{product.price*product.quantity}</td>
				</tr>
			)
		})
		setProductRows(productComponents)
	}, [orderProducts])

	return (
		<div className="container">
			<div className="row">
				<h1 className="text-center my-3">Order Details</h1>
				<p><span className="fw-bold">Order ID:</span> {orderId.substring(10, orderId.length -1)}</p>
				<p><span className="fw-bold">Ordered By:</span> {name}</p>
				<p><span className="fw-bold">Transaction Date:</span> {transactionDate.substring(0,10)}</p>
				<p><span className="fw-bold">Status:</span> {status}</p>
				<Table striped bordered hover className="text-center">
					<thead>
						<tr>
							<th>Product Name</th>
							<th>Quantity</th>
							<th>Subtotal</th>
						</tr>
					</thead>
					<tbody>
						{ productRows }
						<tr>
							<td colSpan="2" className="text-center fw-bold">Total</td>
							<td className="fw-bold">{total}</td>
						</tr>
					</tbody>
				</Table>
			</div>
		</div>
	)
}

