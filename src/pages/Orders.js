import { useState, useEffect, useContext } from 'react'
import { Table } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import UserContext from '../UserContext'

export default function Orders() {

	const {user} = useContext(UserContext);

	const [ordersData, setOrdersData] = useState([]);

	useEffect(() => {
		let fetchUrl = `${process.env.REACT_APP_API_URL}/orders/user-orders`;

		console.log(user.isAdmin === true)
		if (user.isAdmin === true) {
			fetchUrl = `${process.env.REACT_APP_API_URL}/orders`;
		}

		fetch(fetchUrl, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(response => response.json())
		.then(result => {
			console.log(result)

			let data = result.map((order) => {
				return (
					<tr key={order._id}>
						<td>{order._id.substring(10, order._id.length -1)}</td>
						<td>{order.status}</td>
						<td>{order.purchasedOn.substring(0,10)}</td>
						<td>{order.totalAmount}</td>
						<td>
							<Link 
								className="btn btn-primary" to={`/orders/${order._id}`}
							>Details</Link>
						</td>
					</tr>
				)
			})

			setOrdersData(data);
		})
	}, [])

	return(
		<div className="container">
			<div className="row">
				<h1 className="text-center my-3">Orders</h1>
				<Table striped bordered hover className="text-center">
					<thead>
						<tr>
							<th>Order ID</th>
							<th>Status</th>
							<th>Transaction Date</th>
							<th>Total Amount</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						{ ordersData.length > 0 ?
							ordersData
							:
							<tr>
								<td colSpan="5">No orders to display</td>
							</tr>
						}
					</tbody>
				</Table>
			</div>
		</div>
	)
}