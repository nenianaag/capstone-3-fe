import ProductCard from '../components/ProductCard'
import { useEffect, useState, useContext } from 'react'
import UserContext from '../UserContext'
import { Table, Carousel } from 'react-bootstrap'


export default function Products(){

	const {user} = useContext(UserContext)

	const [products, setProducts] = useState([]);
	const [category, setCategory] = useState('');
	const [rerenderProducts, setRerenderProducts] = useState(false);

	
	useEffect(() => {
		let fetchUrl = `${process.env.REACT_APP_API_URL}/products/all-active-products`
		console.log(user.isAdmin)
		if(user.isAdmin === true) {
			
			fetchUrl = `${process.env.REACT_APP_API_URL}/products`
		}

		fetch(fetchUrl, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(response => response.json())
		.then(result => {


			let filteredProducts = result;

			if (category !== "") {
				filteredProducts = filteredProducts.filter((product) => {
					return product.category === category
				})
			}

			const productCards = filteredProducts.map(product => {
				return(
					<ProductCard key={product._id} product={product} rerenderProducts={rerenderProducts} setRerenderProducts={setRerenderProducts}/>
				)
			})
			setProducts(productCards)

		})
	}, [category, rerenderProducts])
	return(
		<>
			<h1 className="text-center my-3">Catalog</h1>
			<div className="container">
				<div className="row justify-content-center mb-5">
					<div className="col-12 col-md-3 col-lg-2">
					<Table striped bordered hover className="text-center" variant="dark">
						<thead>
							<tr>
								<th>Categories</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td onClick={() => setCategory("")}>All Products</td>
							</tr>
							<tr>
								<td onClick={() => setCategory("home-essentials")}>Home Essentials</td>
							</tr>
							<tr>
								<td onClick={() => setCategory("personal-care")}>Personal Care</td>
							</tr>
						</tbody>
					</Table>
					</div>
					<div className="col-12 col-md-9 col-lg-10">
						<div className="carousel">
							<Carousel>
								<Carousel.Item>
									<img
									className="d-block w-100"
									src={process.env.PUBLIC_URL + '/banner-1a.png'}
									alt="First slide"
									/>
								</Carousel.Item>

								<Carousel.Item>
									<img
									className="d-block w-100"
									src={process.env.PUBLIC_URL + '/banner-2a.png'}
									alt="Second slide"
									/>
								</Carousel.Item>

								<Carousel.Item>
									<img
									className="d-block w-100"
									src={process.env.PUBLIC_URL + '/banner-3a.png'}
									alt="Third slide"
									/>
								</Carousel.Item>
							</Carousel>
						</div>	
						<div className="row">
							{products}
						</div>
					</div>			
				</div>
			</div>			
		</>
	)
}