import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2'
import UserContext from '../UserContext'
import { useNavigate, Navigate } from 'react-router-dom';

export default function Profile(){
	const {unsetUser, setUser} = useContext(UserContext)
	const navigate = useNavigate()

	const [email, setEmail] = useState('')
	const [name, setName] = useState('')
	const [mobileNumber, setMobileNumber] = useState('')
	const [password, setPassword] = useState('')
	const [newPassword, setNewPassword] = useState('')
	const [confirmNewPassword, setConfirmNewPassword] = useState('')
	const [formIsValid, setFormIsValid] = useState(false)

	let changePassword = (e) => {

		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/users/change-password`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				password: password,
				newPassword: newPassword
			})
		})
		.then(response => response.json())
		.then(result => {


			if (result === true) {
				Swal.fire({
					title: "Password Changes!",
					icon: 'success',
					text: "Password Successfully Updated"
				})
				setPassword('')
				setNewPassword('')
				setConfirmNewPassword('')
				unsetUser()
				setUser({
					id: null,
					isAdmin: null
				})
				navigate('/login')

			} else {
				Swal.fire({
					title: "Ooppss!",
					icon: 'error',
					text: "Something went wrong. Try checking your password or contact customer support."
				})
			}

		})
	}

	useEffect(()=> {
		fetch(`${process.env.REACT_APP_API_URL}/users/details/info`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(response => response.json())
		.then(result => {

			setEmail(result.email)
			setName(`${result.firstName} ${result.lastName}`)
			setMobileNumber(result.mobileNumber)

		})
	}, [])

	useEffect(() => {
		if(newPassword === confirmNewPassword && newPassword !== '' && password !== '') {
			setFormIsValid(true)
		} else {
			setFormIsValid(false)
		}
	}, [newPassword, confirmNewPassword])

	return (
		<>
			<h1 className="text-center my-3">Profile Page</h1>
			<p className="text-center"><span className="fw-bold">Name:</span> {name}</p>
			<p className="text-center"><span className="fw-bold">Email:</span> {email}</p>
			<p className="text-center"><span className="fw-bold">Mobile Number:</span> {mobileNumber}</p>
			<h3 className="text-center my-3">Change Password</h3>
			<div className="row">
				<Form onSubmit={event => changePassword(event)} className="col-12 col-md-6 offset-md-3 col-lg-6 offset-lg-3 mb-5">
		            <Form.Group controlId="password">
		                <Form.Label>Password</Form.Label>
		                <Form.Control 
			                type="password" 
			                placeholder="Enter current password" 
			                value={password}
			                onChange={event => setPassword(event.target.value)}
			                required
		                />
		            </Form.Group>

		            <Form.Group className="my-3" controlId="new-password">
		                <Form.Label>New Password</Form.Label>
		                <Form.Control 
			                type="password" 
			                placeholder="Enter new password" 
			                value={newPassword}
			                onChange={event => setNewPassword(event.target.value)}
			                required
		                />
		            </Form.Group>

		            <Form.Group className="my-3" controlId="confirm-new-password">
		                <Form.Label>Confirm New Password</Form.Label>
		                <Form.Control 
			                type="password" 
			                placeholder="Confirm new password" 
			                value={confirmNewPassword}
			                onChange={event => setConfirmNewPassword(event.target.value)}
			                required
		                />
		            </Form.Group>

		            <div className="text-center my-3">
			            { formIsValid ? 
			            	<Button variant="success" type="submit" id="submitBtn">
			            		Submit
			            	</Button>
			            	: 
			            	<Button variant="danger" type="submit" id="submitBtn" disabled>
			            		Submit
			            	</Button>
			            }
		            </div>          
		        </Form>
			</div>
			
		</>
	)
}