import { useState, useEffect } from 'react';
import { Table, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function Users() {

	const [usersData, setUsersData] = useState([]);
	const [rerender, setRerender] = useState(false);

	let toggleAdmin = (userId) => {

		fetch(`${process.env.REACT_APP_API_URL}/users/toggle-admin`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				userId: userId
			})
		})
		.then(response => response.json())
		.then(result => {
			if (result === true) {
				
				setRerender(!rerender);

				Swal.fire({
					title: "User updated!",
					icon: 'success',
					text: "User's admin status has been changed!"
				})
			} else {
				Swal.fire({
					title: "Ooppss!",
					icon: 'error',
					text: "Something went wrong"
				})
			}
		})
	}

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/users`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(response => response.json())
		.then(result => {

			let data = result.map((user) => {
				return (
					<tr>
						<td>{user.firstName} {user.lastName}</td>
						<td>{user.isAdmin === true ? 'Admin' : 'Non-Admin'}</td>
						<td>
							{ user.isAdmin === true 
								?
								<Button 
									className="btn btn-danger"
									onClick={() => toggleAdmin(user._id)}
								>Deactivate Admin</Button>
								:
								<Button 
									className="btn btn-success"
									onClick={() => toggleAdmin(user._id)}
								>Activate Admin</Button>
							}
							
						</td>
					</tr>
				)
			})

			setUsersData(data);
		})
	}, [rerender])

	return (
		<div className="container">
			<div className="row">
				<h1 className="text-center my-3">Users Status</h1>
				<Table striped bordered hover className="text-center mb-5">
					<thead>
						<tr>
							<th>Name</th>
							<th>Admin Status</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						{ usersData.length > 0 ?
							usersData
							:
							<tr>
								<td colspan="5">No users to display</td>
							</tr>
						}
					</tbody>
				</Table>
			</div>
		</div>
	)
}